<?php
//-----------------------------------------------------------------------------
// @library        form.inc.php
// @version        1.0
// @date           20.07.2003
// @update         08.01.2007
// @authors        Gabriel Mainberger <gabisoft@freesurf.ch>
// @licence        GPL
//-----------------------------------------------------------------------------
// Liddle CMS - Form Designer
// Copyright (C) 2003-2005 Gabriel Mainberger <gabisoft@freesurf.ch>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//------------------------------------------------------------------------------
//
// History:
//
// 26.10.2003 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - First Publicated Version
// 18.01.2004 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - Add type "smart" to db_form()
// 18.01.2004 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - Add type "select" to db_form()
// 18.01.2004 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - Add type "file" to db_form()
// 21.01.2004 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - Fix </text> to </textarea>
// 25.01.2004 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - Add type "idfile" to db_form()
// 09.02.2004 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - XHTML 1.1 compatible code
// 14.02.2004 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - Add type "htmlarea" to db_form()
// 16.07.2004 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - Add type "heckbox" to db_form()
// 17.10.2005 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - reimplement for better code style ;-)
// 27.11.2005 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - Add element add for insert for insert table only
// 29.01.2006 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - Change to UTF-8
// 08.01.2007 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - Merge Code from pflanzen.winterhart.ch
// 27.08.2013 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - Changed from mysql to mysqli
//
//------------------------------------------------------------------------------

// Feldtyp, Bezeichnung, DB-Name, default, grösse, maximal

function db_form($formname, $table, $fields)
{
	global $id;
	global $mysql_connect_handle;

	$db_form_type_array = array("text", "textarea", "password", "select");
	$d = 0;

	if(!isset($id))
	  if(isset($_REQUEST['id']))
		  $id = $_REQUEST['id'];

	if(isset($_POST[$formname.'_del']))
		unset($id);

	echo "<form accept-charset=\"utf-8\" method=\"post\" action=\"".$_SERVER['PHP_SELF']."\">";

	if(isset($id))
	{
		echo "<input type=\"hidden\" name=\"".$formname."_mod\" value=\"".$id."\" />";
		if(isset($id))
		{
			$sql = "SELECT ";
			$first = 1;

			foreach($fields as $field)
			{
				if(in_array($field[0], $db_form_type_array))
				{
					if(!$first) $sql .= ", ";

					$sql .= $field[2];
					$first = 0;
				}
			}

      	$sql .= " FROM $table WHERE id = $id";
      	$data = db_sql($sql);
		}
	}
	else
		echo "<input type=\"hidden\" name=\"".$formname."_add\" value=\"0\" />\n";

	// hidden fields
	foreach($fields as $field)
	{
		if($field[0]=="hidden" || $field[0]=="add")
			echo "<input type=\"hidden\" name=\"".$formname."_".$field[2]."\" value=\"".$field[3]."\" />\n";
	}

	echo "<table>\n";

	foreach($fields as $field)
	{
		if($field[0]=="hidden" || $field[0]=="add")
			continue;

		echo "<tr>\n";

		if($field[1]!="say")
			echo "  <td>".$field[1]."</td>\n";

		if(($field[0]=="text") or ($field[0]=="password"))
			echo "  <td><input type=\"".$field[0]."\" name=\"".$formname."_".$field[2]."\" size=\"".$field[4]."\" value=\"".(isset($id)?$data[$d++]:$field[3])."\" /></td>\n";
		else if($field[0]=="textarea")
			echo "  <td><textarea name=\"".$formname."_".$field[2]."\" cols=\"".$field[4]."\" rows=\"".$field[5]."\">".(isset($id)?$data[$d++]:$field[3])."</textarea></td>\n";
		else if($field[0]=="select")
		{
			$selectedid = $data[$d++];
			echo "  <td><select name=\"".$formname."_".$field[2]."\" size=\"1\">";
			$result = @mysqli_query($mysql_connect_handle, $field[3]);
			while($row=@mysqli_fetch_row($result))
			{
				if($row[0]==$selectedid)
					echo "<option selected=\"selected\" value=\"".$row[0]."\">".$row[1]."</option>";
				else
					echo "<option value=\"".$row[0]."\">".$row[1]."</option>";
			}
			@mysqli_free_result($result);
			echo "</select></td>";
		}
		else
			echo "  <td colspan=\"2\">".$field[3]."</td>\n";

		echo "</tr>\n";
	}

	echo "</table><br />\n";

	if(isset($id))
	{
		echo "<input type=\"submit\" value=\"Ändern\" accesskey=\"s\" /></form>";
		echo "<form method=\"post\" action=\"".$_SERVER['PHP_SELF']."\"><input type=\"hidden\" name=\"".$formname."_del\" value=\"$id\" /><input type=\"submit\" value=\"Löschen\" /></form>";
	}
	else
		echo "<input type=\"submit\" value=\"Einfügen\" accesskey=\"s\" /></form>";
}

function db_add($formname, $table, $fields, $text="")
{
	global $id;
	global $mysql_connect_handle;


	$db_add_type_array = array("text", "textarea", "password", "select", "hidden", "add");

	if(isset($_POST[$formname.'_add']))
	{
		$sql = "INSERT INTO $table (";

		$first = 1;
		foreach($fields as $field)
		{
			if(in_array($field[0], $db_add_type_array))
			{
				if(!$first)	$sql .= ", ";

				$sql .= $field[2];
				$first = 0;
			}
		}

		$sql .= ") VALUES (";

		$first = 1;
		foreach($fields as $field)
		{
      	if(in_array($field[0], $db_add_type_array))
			{
				if(!$first) $sql .= ", ";

				$sql .= "'".$_POST[$formname."_".$field[2]]."'";
				$first = 0;
			}
		}

		$sql .= ")";

		if(!@mysqli_query($mysql_connect_handle, $sql))
			errormsg("Konnte Daten nicht in die Datenbank eintragen!");

		if($text!="")
		{
			echo $text;
			page_end();
			exit;
		}

		$id = mysqli_insert_id($mysql_connect_handle);
	}
}

function db_mod($formname, $table, $fields)
{
	global $id;
	global $mysql_connect_handle;

	$db_mod_type_array = array("text", "textarea", "password", "select", "hidden");

	if(isset($_POST[$formname.'_mod']))
	{
		$sql = "UPDATE $table SET ";

		$first = 1;
		foreach($fields as $field)
		{
			if(in_array($field[0], $db_mod_type_array))
			{
				if(!$first) $sql .= ", ";

				$sql .= $field[2]." = '".$_POST[$formname."_".$field[2]]."'";
				$first = 0;
			}
		}

		$sql .= " WHERE id = ".$_POST[$formname.'_mod'];

		if(!@mysqli_query($mysql_connect_handle, $sql))
			errormsg(mysqli_get_charset($mysql_connect_handle) + "Konnte Daten nicht ändern!");



		$id = $_POST[$formname.'_mod'];
	}
}

function db_del($formname, $table)
{
	global $mysql_connect_handle;
	global $id;

	if(isset($_POST[$formname.'_del']))
	{
		$sql = "DELETE FROM $table WHERE id = ".$_POST[$formname.'_del'];

		if(!@mysqli_query($mysql_connect_handle, $sql))
			errormsg("Konnte Daten nicht ändern!");
	}
}

?>
