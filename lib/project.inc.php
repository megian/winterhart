<?php
//-----------------------------------------------------------------------------
// @library        project.inc.php
// @version        1.0
// @date           2.6.2003
// @update         26.10.2003
// @authors        Gabriel Mainberger <gabisoft@freesurf.ch>
// @licence        GPL
//-----------------------------------------------------------------------------
// Copyright (C) 2003 Gabriel Mainberger <gabisoft@freesurf.ch>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// @function        menu_entries()
// @paramter        nichts
// @return        nichts
// @description        Enthält den Inhalt des Menus
//-----------------------------------------------------------------------------

function nav_entries()
{?>
<ul>
	<li><a href="index.php">Informationen</a></li>
	<li><a href="search.php">Pflanzen-Suche</a></li>
	<li><a href="list.php">Pflanzen-Liste</a></li>
	<li><a href="insert.php">Eintragen</a></li>
	<li><a href="contact.php">Kontakt</a></li>
	<li><a href="link.php">Links</a></li>
</ul>
<?php }

function page_begin($htmltitle)
{
	db_open();
	html_begin($htmltitle);
	
	// TITLE
	$title = "www.winterhart.ch";
	echo '<header id="title"><span class="title-text">'.$title.'</span></header>';

	// NAV BEGIN
	echo '<nav id="menu">';
	echo '<div id="menu-title">☰</div>';
	echo '<div id="menu-content">';
	
	// MENU ENTRIES
	nav_entries();
	
	// NAV END
	echo '</div>';
	echo '<div id="menu-bottom"></div>';
	echo '</nav>';
	
	// MAIN
	echo '<main id="content">';
	echo '<div class="content-title">'.$title.'</div>';
}

function page_end()
{
	echo "</main>";
	html_end();
}

?>
