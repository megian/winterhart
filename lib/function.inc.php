<?php
//-----------------------------------------------------------------------------
// @library        function.inc.php
// @version        1.0
// @date           2.6.2003
// @update         22.12.2021
// @authors        Gabriel Mainberger <gabisoft@freesurf.ch>
// @licence        GPL
//-----------------------------------------------------------------------------
// General PHP-Functions for a liddle CMS
// Copyright (C) 2003-2021 Gabriel Mainberger <gabisoft@freesurf.ch>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//------------------------------------------------------------------------------
//
// History:
//
// 26.10.2003 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - First Publicated Version
// 27.08.2013 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - Changed from mysql to mysqli
// 19.12.2021 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - PHP8+HTML5 migration
//
//------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Deutsche Monatsnamen in einem Array
//-----------------------------------------------------------------------------

$monthname = array("", "Januar", "Februar", "März", "April", "Mai", "Juni",
"Juli", "August", "September", "Oktober", "November", "Dezember");

//-----------------------------------------------------------------------------
// @function        errormsg()
// @paramter        Fehlermeldung
// @return        nichts
// @description        Gibt eine Fehlermeldung aus und bricht die Ausführung
//                der Script Ausführung ab.
//-----------------------------------------------------------------------------

function errormsg($msg) {
  html_begin();
  echo "ERROR: $msg";
  html_end();
  exit;
}

//-----------------------------------------------------------------------------
// @function        warnmsg()
// @paramter        Warnung
// @return        nichts
// @description        Gibt eine Warnmeldung aus und führt das Script weiter aus.
//-----------------------------------------------------------------------------

function warnmsg($msg) {
  echo $msg;
}

//-----------------------------------------------------------------------------
// @function        html_begin()
// @paramter        Tiel der Seite (optional)
// @return        nichts
// @description        Erstellt den Grundaufbau der HTML Seite
//-----------------------------------------------------------------------------

function html_begin($title="Noname") {
  echo <<<___HTML___
  <!DOCTYPE html>
  <html lang="de">
  <head>
    <meta charset="utf-8">
    <title>$title</title>
    <link rel="stylesheet" title="Default Style" type="text/css" href="default.css">
  </head>
  <body>
  ___HTML___;
}

//-----------------------------------------------------------------------------
// @function        html_end()
// @paramter        nichts
// @return        nichts
// @description        Beendet die HTML Seite
//-----------------------------------------------------------------------------

function html_end() {
	echo '</body></html>';
}

//-----------------------------------------------------------------------------
// @function        frame_begin()
// @paramter        Titel, Titel-Farbe (optional), Hintergrundfarbe (optional)
// @return        nichts
// @description        Rahmen
//-----------------------------------------------------------------------------

function frame_begin($title, $color_title="#663366", $color_body="#ffffff")
{?>
<table cellpadding="0" cellspacing="1" border="0" width="100%" bgcolor="<?php echo $color_title; ?>">
<tr><td>
<table cellpadding="2" cellspacing="0" border="0" width="100%">
<tr><td><b><?php echo $title; ?></b></td></tr><tr><td bgcolor="<?php echo $color_body; ?>">
<?php }

//-----------------------------------------------------------------------------
// @function        frame_end()
// @paramter        nichts
// @return        nichts
// @description        Rahmen-Ende
//-----------------------------------------------------------------------------

function frame_end()
{?>
</td></tr></table></td></tr></table>
<?php }

//-----------------------------------------------------------------------------
// @function      db_open()
// @paramter      nichts
// @return        nichts
// @description   Stellt die Verbindung zur Datenbank her und setzt die
//                Standard Datenbank. Die Paramter die benötigt werden, werden
//                in der config.inc.php konfiguriert.
//                Der Datenbank-Handle ist in der globalen Variable
//                $mysql_connect_handle vorhanden.
//-----------------------------------------------------------------------------

function db_open() {
  global $config_mysql_hostname;
  global $config_mysql_username;
  global $config_mysql_password;
  global $config_mysql_database;
  global $mysql_connect_handle;

  $mysql_connect_handle = mysqli_connect($config_mysql_hostname, $config_mysql_username, $config_mysql_password, $config_mysql_database);

  if(mysqli_connect_errno())
    errormsg("mysqli_connect(): Konnte Datenbankverbindung nicht herstellen!" + mysqli_connect_error());

  /* change character set to utf8 */
  if (!mysqli_set_charset($mysql_connect_handle, "utf8"))
  {
    printf("Error loading character set utf8: %s\n", mysqli_error($mysql_connect_handle));
  }
}


//-----------------------------------------------------------------------------
// @function        db_sql()
// @parameter       SQL
// @return          Array mit allen Daten
// @description     Fragt mit einem SQL Befahl die Datenbank ab und gibt das
//                  Resultat in einem Arry zurück.
//-----------------------------------------------------------------------------

function db_sql($sql) {
  global $mysql_connect_handle;

  $result = mysqli_query($mysql_connect_handle, $sql);

  if($row = mysqli_fetch_row($result)) {
    mysqli_free_result($result);
    return($row);
  } else
    warnmsg("db_sql(): Konnte SQL-Statement nicht ausfuehren!");
}

//-----------------------------------------------------------------------------
// @function      db_sql_multi()
// @paramter      SQL, Aufzurufende-Funktion
// @return        nichts
// @description   Fragt mit einem SQL Befahl die Datenbank ab und ruft für jeden
//                Datensatz die angegebene Funktion auf.
//-----------------------------------------------------------------------------

function db_sql_multi(string $sql, string $func): void {
  global $mysql_connect_handle;

  $result = mysqli_query($mysql_connect_handle, $sql);

  while($row = mysqli_fetch_row($result))
    call_user_func($func, $row);

  mysqli_free_result($result);
}

//-----------------------------------------------------------------------------
// @function        db_sql_table()
// @paramter        SQL, Titel der Tabelle
// @return        nichts
// @description        Fragt mit einem SQL Befahl die Datenbank ab und erstellt eine
//                Tabelle. Die Titel und die Spaltenbreiten, werden durch
//                $titles definiert.
//                titles: Anzeige Name:Spaltenbreite:PHP-Datei
//-----------------------------------------------------------------------------

function db_sql_table($sql, $titles)
{
  global $mysql_connect_handle;
?>
<table width="100%">
<tr>
<?php
  $t = preg_split(":", $titles);
  $ti = count($t);
  for($i=0;$i<$ti;$i+=3)
    echo "  <th width=\"".$t[$i+1]."\">".$t[$i]."</th>\n";
  echo "</tr>\n";

  $ti = (int)($ti/3);

  if(!$result = mysqli_query($mysql_connect_handle, $sql))
    warnmsg("db_sql_table(): Konnte Query nicht ausführen: $sql");

  while($row = mysqli_fetch_row($result))
  {
    for($i=1;$i<=$ti;$i++)
    {
      if($t[$i*3-1]=="")
        echo "  <td>".$row[$i]."</td>\n";
      else
        echo "  <td><a href=\"".$t[$i*3-1].$row[0]."\">".$row[$i]."</td>\n";
    }
    echo "</tr>\n";
    $ti2--;
  }
  mysqli_free_result($result);
  echo "</table>\n";
}

//-----------------------------------------------------------------------------
// @function        db_sql_table_orderby()
// @paramter        SQL-Syntax, Titel der Tabelle, Sortiertung
// @return        nichts
// @description        Fragt mit einem SQL Befahl die Datenbank ab und erstellt eine
//                Tabelle. Die Titel und die Spaltenbreiten, werden durch
//                $titles definiert.
//                titles: Anzeige Name:Datenbank Feld:Spaltenbreite:PHP-Datei
//-----------------------------------------------------------------------------

function db_sql_table_orderby($sql, $titles, $orderby)
{
  global $mysql_connect_handle;
?>
<table width="100%">
<tr>
<?php
  $t = preg_split(":", $titles);
  $ti = count($t);
  for($i=0;$i<$ti;$i+=4)
    echo "  <th width=\"".$t[$i+2]."\"><a href=\"".htmlentities($_SERVER['PHP_SELF'])."?o=".($i/4)."\">".$t[$i]."</a></th>\n";
  echo "</tr>\n";

  $ti = (int)($ti/4);

  if($orderby!="")
    $sql .= " ORDER BY ".$t[$orderby*4+1];

  $result = mysqli_query($mysql_connect_handle, $sql);

  while($row = mysqli_fetch_row($result))
  {
    echo "<tr>\n";
    for($i=1;$i<=$ti;$i++)
    {
      if($t[$i*4-1]=="")
        echo "  <td>".$row[$i]."</td>\n";
      else
        echo "  <td><a href=\"".$t[$i*4-1]."?id=".$row[0]."\">".$row[$i]."</td>\n";
    }
    echo "</tr>\n";
    $ti2--;
  }
  mysqli_free_result($result);
  echo "</table>\n";
}

//-----------------------------------------------------------------------------
// @function        db_navigation()
// @paramter        Datenbank Tabellenname, Limit
// @return        nichts
// @description        Erstellt eine Navigationsleiste
//-----------------------------------------------------------------------------

function db_navigation($table, $limit, $where='')
{
  global $mysql_connect_handle;

  if($result = mysqli_query($mysql_connect_handle, "SELECT COUNT(*) FROM $table".($where!=''?" WHERE $where":''))) {
    $row_result = mysqli_fetch_row($result);
	  mysqli_free_result($result);
	  $rows = $row_result[0];

    $rows = (int)($rows / $limit);

    for($i=0;$i<=$rows;$i++)
      echo "<a href=\"?o=$i\">[".($i+1)."]</a> ";
  }
}

//-----------------------------------------------------------------------------
// @function        db_navigation_sql()
// @paramter        SQL, Limit
// @return        nichts
// @description        Erstellt eine Navigationsleiste
//-----------------------------------------------------------------------------

function db_navigation_sql($sql, $limit)
{
  global $mysql_connect_handle;

  $result = mysqli_query($mysql_connect_handle, $sql);
  $rows = mysqli_num_rows($result);
  mysqli_free_result($result);

  $rows = (int)($rows / $limit);

  for($i=0;$i<=$rows;$i++)
    echo "<a href=\"?o=$i\">[".($i+1)."]</a> ";
}

//-----------------------------------------------------------------------------
// @function        db_select()
// @paramter        Form Name, SQL, aktueller Datensatz
// @return        nichts
// @description        Erstellt eine Combobox mit Daten aus der Datenbank
//-----------------------------------------------------------------------------

function db_select($name, $sql, $sid)
{
  global $mysql_connect_handle;

  $result = mysqli_query($mysql_connect_handle, $sql);
  echo "<select name=\"$name\">\n";

  while($row = mysqli_fetch_row($result))
{?>
<option value="<?php echo $row[0]; ?>"<?php if($row[0]==$sid) echo " selected=\"selected\""; ?> ><?php echo $row[1]?></option>
<?php }

  echo "</select>\n";
  mysqli_free_result($result);
}

//-----------------------------------------------------------------------------
// @function        db_page_show()
// @paramter        SQL, Funktion, aktuelle Tabelle, Limit, Offset
// @return        nichts
// @description        Erstellt eine Seite mit Navigationsbalken und Daten
//-----------------------------------------------------------------------------

function db_page_show($sql, $func, $table, $limit, $offset)
{
  $sql .= " LIMIT ".($offset*$limit).",$limit";

  db_navigation($table, $limit);
  echo "<br><br>\n";
  db_sql_multi($sql, $func);
  echo "<br><br>\n";
  db_navigation($table, $limit);
}
?>
