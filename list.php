<?php
require('default.inc.php');
page_begin('Liste');

echo '<h1>Liste</h1>';

function show($data)
{
  echo "<tr><td>$data[0]</td><td>$data[1]</td><td>$data[2]</td><td>$data[3]</td><td><a href=\"https://map.search.ch/".$data[4]."\">$data[4]</a></td><td><a href=\"https://map.search.ch/".$data[4]."\">$data[5]</a></td><td>$data[6]</td><td>$data[7]</td><td>$data[8]</td></tr>";
}

$o = $_GET['o'] ?? '';

$limit = 50;
$table = 'liste';
$where = 'liste.status = 1';

echo '<p>';
db_navigation($table, $limit, $where);
echo '</p>';

echo <<<___HTML___
<table class="sqltable">
<tr>
  <th>Gattung</th>
  <th>Art</th>
  <th>Sorte</th>
  <th>Deutscher Name</th>
  <th>PLZ</th>
  <th>Ort</th>
  <th>Höhe</th>
  <th>Pflanzjahr</th>
  <th>Kältezone(n)</th>
</tr>
___HTML___;

db_sql_multi("SELECT liste.gattung, liste.art, liste.sorte, liste.dname, liste.plz, plz.ort, liste.hoehe, liste.pflanzjahr, liste.zone FROM $table LEFT JOIN plz ON (plz.plz = liste.plz) WHERE $where ORDER BY liste.gattung LIMIT ".($o*$limit).",$limit", show);
echo '</table>';

echo '<p>';
db_navigation($table, $limit);
echo '</p>';

page_end(); 
?>