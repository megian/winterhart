<?php
require('default.inc.php');
page_begin('Pflanzen-Suche');

// Input validation
$search_index = [
  'name',
  'plz',
  'tpflanzjahr',
  'hpflanzjahr',
  'thoehe',
  'hhoehe',
];
foreach ($search_index as $index)
  $search[$index] = $_GET[$index] ?? '';

$table = 'liste';

echo '<h1>Suche</h1>';
echo '<form accept-charset="utf-8" method="get" action="'.htmlentities($_SERVER['PHP_SELF']).'">';
?>
<table>
<tr>
  <td>Name</td><td><input name="name" size="50" value="<?php echo $search['name']; ?>"></td>
</tr>
<tr>
  <td>PLZ</td><td><input name="plz" size="4" value="<?php echo $search['plz']; ?>"></td>
</tr>
<tr>
  <td>Pflanzjahr von</td>
  <td><input name="tpflanzjahr" size="4" value="<?php echo $search['tpflanzjahr']; ?>"> bis <input name="hpflanzjahr" size="4" value="<?php echo $search['hpflanzjahr']; ?>"></td>
</tr>
<tr>
  <td>Höhe von</td>
  <td><input name="thoehe" size="4" value="<?php echo $search['thoehe']; ?>"> m bis <input name="hhoehe" size="4" value="<?php echo $search['hhoehe']; ?>"> m</td>
</tr>
<tr>
  <td></td>
  <td><input type="submit" value="Suchen" accesskey="s"> <input type="reset" value="Zurücksetzen"></td>
</tr>
</table>
</form>
<br>
<?php

function show($data)
{
  echo "<tr><td>$data[0]</td><td>$data[1]</td><td>$data[2]</td><td>$data[3]</td><td><a href=\"https://map.search.ch/".$data[4]."\">$data[4]</a></td><td><a href=\"https://map.search.ch/".$data[4]."\">$data[5]</a></td><td>$data[6]</td><td>$data[7]</td><td>$data[8]</td></tr>";
}

if(($search['name']!="") || ($search['plz']!="") || ($search['tpflanzjahr']!="") || ($search['hpflanzjahr']!="") || ($search['thoehe']!="") || ($search['hhoehe']!=""))
{
  echo <<<___HTML___
  <table class="sqltable">
  <tr>
    <th>Gattung</th>
    <th>Art</th>
    <th>Sorte</th>
    <th>Deutscher Name</th>
    <th>PLZ</th>
    <th>Ort</th>
    <th>Höhe</th>
    <th>Pflanzjahr</th>
    <th>Kältezone(n)</th>
  </tr>
  ___HTML___;
  
  $sql = "SELECT $table.gattung, $table.art, $table.sorte, $table.dname, $table.plz, plz.ort, $table.hoehe, $table.pflanzjahr, $table.zone FROM $table LEFT JOIN plz ON (plz.plz = $table.plz) WHERE";

  $t = 0;

  if($search['name']!="")
  {
    if($t)
      $sql .= " AND";
    else
      $t = 1;

    $sql .= " ($table.gattung LIKE '%".$search['name']."%' OR $table.art LIKE '%".$search['name']."%' OR $table.sorte like '%".$search['name']."%' OR $table.dname LIKE '%".$search['name']."%')";
    $t = 1;
  }

  if($search['plz']!="")
  {
    if($t)
      $sql .= " AND";
    else
      $t = 1;

    $sql .= " plz.plz = ".$search['plz'];
  }
  
  if(($search['hpflanzjahr']!=""))
  {
    if($t)
      $sql .= " AND";
    else
    	$t = 2;      

    $sql .= " $table.pflanzjahr <= ".$search['hpflanzjahr'];
  }

  if(($search['tpflanzjahr']!=""))
  {
    if($t)
      $sql .= " AND";
    else
      $t = 2;

    $sql .= " $table.pflanzjahr >= ".$search['tpflanzjahr'];
  }

  if(($search['hhoehe']!=""))
  {
    if($t)
      $sql .= " AND";
    else
    	$t = 2;      

    $sql .= " $table.hoehe <= ".$search['hhoehe'];
  }

  if(($search['thoehe']!=""))
  {
    if($t)
      $sql .= " AND";
    else
      $t = 2;

    $sql .= " $table.hoehe >= ".$search['thoehe'];
  }
  
  db_sql_multi($sql, 'show');
  echo '</table>';
}

page_end(); 
?>