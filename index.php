<?php
require('default.inc.php');
page_begin('Informationen');
?>

<h1>Herzlich Willkommen!</h1>

<img src="img/fruechte.jpg" alt="fruechte">

<p>Herzlich Willkommen auf winterhart.ch. Der neuen Schweizer Plattform für winterharte Pflanzen.</p> 

<h2>1.Welche Pflanzen werden in die Liste aufgenommen</h2>
<p>Grundzätzlich werden nur Pflanzen aufgenommen, welche mindestens 5 Jahre im Freiland in der Schweiz gewachsen sind. 
(Wintergarten, Keller, ect. sind ausgeschlossen)<br>
Wichtig ist, das nur Pflanzen ab der Kältezone 6 (siehe weiter unten) in der Liste aufgeführt werden, damit wir möglichst genau die Grenzkältezonen der untengenannten Pflanzen herausfinden können.</p>
     
<p>Wir nehmen folgende Kategorien auf:</p>

<ul>
 <li>Gehölze</li>
 <li>Kübelpflanzen</li>
 <li>Palmen</li>
 <li>Fruchtpflanzen</li>
</ul>

<h2>2. Wie kann man eine Pflanze registrieren</h2>

<p>Klicken Sie auf "Eintragen" und tragen sie die Informationen möglichst genau ein.</p>

<h2>3. Abmelden von Pflanzen</h2>

<p>Bitte senden sie einen kurzes E-Mail an abmeldung@winterhart.ch. Damit können wir die Liste aktuell halten.</p>

<h2>4. Pflanzensuche</h2>

<p>Alle registrierten Pflanzen könne mit der "Pflanzen-Suche" oder über die "Pflanzen-Liste" durchsucht werden.</p>

<h2>5. Kältezonen</h2>

<p>Auf der Homepage wird auf Kältezonen hingewiesen. Die Temperatur-Bereiche können aus der folgenden Tabelle bezogen werden:</p>

<p>
Zone 1 unter -45° C<br>
Zone 2 -45 bis -40° C<br>
Zone 3 -40 bis -34° C<br>
Zone 4 -34 bis -29° C<br>
Zone 5 -29 bis -23° C<br>
Zone 6 -23 bis -17° C<br>
Zone 7 -17 bis -12° C<br>
Zone 8 -12 bis -7° C<br>
Zone 9 -7 bis -1° C<br>
Zone 10 -1 bis 5° C<br>
</p>

<h2>6. Diverses</h2>
<p>Der Inhalt dieser Internetseite wird nach bestem Wissen bearbeitet, jedoch wird jegentliche Haftung für ausgepflanzte Pflanzen abgewiesen, welche einen Schaden genommen haben.</p>
      
<p>Es gibt keinen Postversand, jegentliche Korespondenz wird über E-Mail oder über die Internetseite abgewickelt.</p>

<p></p>
   
<p>Wir wünschen Ihnen viel Vergnügen.</p>

<p><i>Ihr Winterhart Team</i></p>

<?php page_end(); ?>