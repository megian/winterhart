<?php
require('default.inc.php');
page_begin('Hinzufügen'); 

echo '<h1>Hinzufügen</h1>';
echo '<p><a href="addform.php">Pflanze selber hinzufügen</a></p>';

function show($data)
{
	echo "<tr onclick=\"location.href='addform.php?id=$data[0]'\"><td><a href=\"addform.php?id=$data[0]\"><img src=\"img/edit.png\" alt=\"Edit\"></a></td><td>$data[1]</td><td>$data[2]</td><td>$data[3]</td><td>$data[4]</td><td>$data[5]</td></tr>";
}

echo <<<___HTML___
<table class="sqltable">
<tr>
  <th style="width:10%"></th>
	<th style="width:20%">Gattung</th>
  <th style="width:20%">Art</th>
  <th style="width:20%">Sorte</th>
  <th style="width:10%">PLZ</th>
  <th style="width:10%">Höhe</th>
</tr>
___HTML___;

db_sql_multi("SELECT id, gattung, art, sorte, plz, hoehe FROM liste WHERE status=0", 'show');

echo '</table>';

page_end();
?>