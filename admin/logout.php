<?php
require('default.inc.php');
session_start();
$_SESSION['login_state'] = 'FALSE';
unset($_SESSION['username']);
unset($_SESSION['userid']);
unset($_SESSION['name']);
session_destroy();

if(strtolower(session_module_name()) == 'files')
{
  unlink(session_save_path().'sess_'.session_id());
}

html_begin('Logout');
title('');
menu_begin('Logout', 'menu-login');
echo '<a href="index.php" accesskey="z">Zurück</a>';
menu_end();
body_middle();
echo '<p>Sie sind nun ausgeloggt! Bitte erneut einloggen...</p>';
body_end();
html_end();
?>