<?php
require('default.inc.php');
page_begin('Links');

$versiondate = date("Y-m-d H:i:s");

$fields = array(
  array("text", "Datum", "versiondate", $versiondate, 50),
  array("select", "Kategorie", "categorie", "SELECT id, name FROM linkscategorie"),
  array("text", "Title", "title", "", 50),
  array("text", "Link", "link", "", 50),
);

echo '<h1>Links</h1>';

db_add("form1", "links", $fields, "");
db_mod("form1", "links", $fields);
db_del("form1", "links");
db_form("form1", "links", $fields);

page_end();
?>