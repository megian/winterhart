<?php
require('default.inc.php');
page_begin('Adressen');

echo '<h1>Adressen</h1>';

function show($data)
{
	echo "<tr onclick=\"location.href='listform.php?id=$data[0]'\"><td><a href=\"listform.php?id=$data[0]\"><img src=\"img/edit.png\" alt=\"Edit\"></a></td><td>$data[1]</td><td>$data[2]</td><td>$data[3]</td><td>$data[4]</td><td><a href=\"https://map.search.ch/".$data[5]."\">$data[5]</a></td><td><a href=\"https://map.search.ch/".$data[5]."\">$data[6]</a></td><td>$data[7]</td></tr>";
}

$limit = 25;
$table = "liste";
$o = $_GET['o'] ?? 0;

echo '<p>';
db_navigation_sql("SELECT id, anrede, vorname, nachname, adresse, adrplz, ort, email FROM $table WHERE nachname <> '' and status = 1", $limit);
echo '</p>';

echo "<table class=\"sqltable\">";
echo "<tr>";
echo "<th></th>";
echo "<th>Anrede</th>";
echo "<th>Vorname</th>";
echo "<th>Nachname</th>";
echo "<th>Adresse</th>";
echo "<th>PLZ</th>";
echo "<th>Ort</th>";
echo "<th>E-Mail</th>";
echo "</tr>";

db_sql_multi("SELECT id, anrede, vorname, nachname, adresse, adrplz, ort, email FROM $table WHERE nachname <> '' LIMIT ".($o*$limit).",$limit", 'show');
echo "</table>";

echo "<p>";
db_navigation_sql("SELECT id, anrede, vorname, nachname, adresse, adrplz, ort, email FROM $table WHERE nachname <> '' and status = 1", $limit);
echo "</p>";

page_end();
?>