<?php
require('default.inc.php');
page_begin('Suchen');

// Input validation
$search_index = [
  'name',
  'plz',
  'tpflanzjahr',
  'hpflanzjahr',
  'thoehe',
  'hhoehe',
];
foreach ($search_index as $index)
  $search[$index] = $_GET[$index] ?? '';

echo '<h1>Suchen</h1>';
echo '<form accept-charset="utf-8" method="get" action="'.htmlentities($_SERVER['PHP_SELF']).'">';
?>
<table>
<tr>
  <td>Name</td><td><input name="name" size="50"  value="<?php echo $search['name']; ?>"></td>
</tr>
<tr>
  <td>PLZ</td><td><input name="plz" size="4" value="<?php echo $search['plz']; ?>"></td>
</tr>
<tr>
  <td>Pflanzjahr von</td>
  <td><input name="tpflanzjahr" size="4" value="<?php echo $search['tpflanzjahr']; ?>"> bis <input name="hpflanzjahr" size="4" value="<?php echo $search['hpflanzjahr']; ?>"></td>
</tr>
<tr>
  <td>Höhe von</td>
  <td><input name="thoehe" size="4" value="<?php echo $search['thoehe']; ?>"> m bis <input name="hhoehe" size="4" value="<?php echo $search['hhoehe']; ?>"> m</td>
</tr>
<tr>
  <td></td>
	<td><input type="submit" value="Suchen" accesskey="s"> <input type="reset" value="Zurücksetzen"></td>
</tr>
</table>
</form>
<br>
<?php

function show($data)
{
  echo "<tr onclick=\"location.href='listform.php?id=$data[0]'\"><td><a href=\"listform.php?id=".$data[0]."\"><img src=\"img/edit.png\" alt=\"Edit\"></a></td><td>$data[1]</td><td>$data[2]</td><td>$data[3]</td><td>$data[4]</td><td><a href=\"http://www.swissgeo.ch/adresse_search.php?npa=".$data[5]."\">$data[5]</a></td><td><a href=\"http://www.swissgeo.ch/adresse_search.php?npa=".$data[5]."\">$data[6]</a></td><td>$data[7]</td><td>$data[8]</td><td>$data[9]</td></tr>";
}

if(($search['name']!="") || ($search['plz']!="") || ($search['tpflanzjahr']!="") || ($search['hpflanzjahr']!="") || ($search['thoehe']!="") || ($search['hhoehe']!=""))
{
  $table = 'liste';
  $columns = [
    [
			'caption' => '',
			'database' => "$table.id",
		],
		[
			'caption' => 'Gattung',
			'database' => "$table.gattung",
		],
		[
			'caption' => 'Art',
			'database' => "$table.art",
		],
		[
			'caption' => 'Sorte',
			'database' => "$table.sorte",
		],
		[
			'caption' => 'Deutscher Name',
			'database' => "$table.dname",
		],
		[
			'caption' => 'PLZ',
			'database' => "$table.plz",
		],
		[
      'caption' => 'Ort',
      'database' => 'plz.ort',
		],
		[
			'caption' => 'Höhe',
			'database' => "$table.hoehe",
		],
		[
			'caption' => 'Pflanzjahr',
			'database' => "$table.pflanzjahr",
		],
		[
			'caption' => 'Kältezone(n)',
			'database' => "$table.zone",
		],
	];

	echo '<table class="sqltable">';
	echo '<tr>';
	  foreach ($columns as $column)
		  echo '<th>'.($column['caption'] ?? 'NICHTDEFINIERT').'</th>';
	echo '</tr>';

	$sql = 'SELECT ';
	foreach ($columns as $column) {
		if ($column !== reset($columns))
		  $sql .= ',';
  	$sql .= $column['database'];
	}
	$sql .= ' FROM '.$table.' LEFT JOIN plz ON (plz.plz = liste.plz) WHERE ';

	$t = 0;

	if($search['name']!="")
	{
		$sql .= " (gattung like '%".$search['name']."%' or art like '%".$search['name']."%' or sorte like '%".$search['name']."%' or dname like '%".$search['name']."%')";
		$t = 1;
	}

	if($search['plz']!="")
	{
		if($t)
			$sql .= " and";
		else
      	$t = 1;

		$sql .= " plz.plz = ".$search['plz'];
	}
  
	if(($search['hpflanzjahr']!=""))
	{
		if($t)
			$sql .= " and";
		else
			$t = 2;      

		$sql .= " liste.pflanzjahr <= ".$search['hpflanzjahr'];
	}

	if(($search['tpflanzjahr']!=""))
	{
		if($t)
			$sql .= " and";
		else
			$t = 2;

		$sql .= " liste.pflanzjahr >= ".$search['tpflanzjahr'];
	}

	if(($search['hhoehe']!=""))
	{
		if($t)
			$sql .= " and";
		else
			$t = 2;      

		$sql .= " liste.hoehe <= ".$search['hhoehe'];
	}

	if(($search['thoehe']!=""))
	{
		if($t)
			$sql .= " and";
		else
			$t = 2;

		$sql .= " liste.hoehe >= ".$search['thoehe'];
	}

	db_sql_multi($sql, 'show');
	echo '</table>';
}

page_end(); 
?>