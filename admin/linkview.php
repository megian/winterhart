<?php
require('default.inc.php');
page_begin('Links');

echo '<h1>Links</h1>';
echo '<p><a href="linkform.php">Neuer Link</a></p>';

function show($data)
{
  echo "<tr onclick=\"location.href='linkform.php?id=$data[0]'\"><td><a href=\"linkform.php?id=".$data[0]."\"><img src=\"img/edit.png\" alt=\"Edit\" /></a></td><td>$data[1]</td><td>$data[2]</td><td>$data[3]</td><td><a href=\"$data[4]\">$data[4]</a></td></tr>";
}

$limit = 50;
$table = "links";
$o = $_GET['o'] ?? 0;

echo "<p>";
db_navigation($table, $limit);
echo "</p>";

echo "<table class=\"sqltable\">";
echo "<tr>";
echo "<th></th>";
echo "<th>Datum</th>";
echo "<th>Kategorie</th>";
echo "<th>Titel</th>";
echo "<th>Link</th>";
echo "</tr>";

db_sql_multi("SELECT $table.id, $table.versiondate, linkscategorie.name, $table.title, $table.link FROM $table LEFT JOIN linkscategorie ON (linkscategorie.id = links.categorie) LIMIT ".($o*$limit).",$limit", 'show');
echo "</table>";

echo '<p>';
db_navigation($table, $limit);
echo '</p>';

page_end(); 
?>