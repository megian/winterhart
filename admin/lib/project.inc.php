<?php
//-----------------------------------------------------------------------------
// @library        project.inc.php
// @date           2003-06-02
// @update         2021-12-22
// @authors        Gabriel Mainberger <gabisoft@freesurf.ch>
//-----------------------------------------------------------------------------

function menu_entries(): void { ?>
<ul>
	<li><a href="index.php">Home</a></li>
	<li><a href="adminview.php">Administratoren</a></li>
	<li><a href="addview.php">Hinzufügen</a></li>
	<li><a href="search.php">Suchen</a></li>
	<li><a href="listview.php">Liste</a></li>
	<li><a href="addressview.php">Adressen</a></li>
	<li><a href="linkcategorieview.php">Link Kategorien</a></li>
	<li><a href="linkview.php">Links</a></li>
	<li><a href="export.php">CSV Export</a></li>
	<li><a href="logout.php">Logout</a></li>
</ul>
<?php }

function page_begin(string $htmltitle): void {
  db_open();
  session();
  html_begin($htmltitle);
	$username = $_SESSION['username'] ?? 'Undefiniert';
  title("www.winterhart.ch - User: ".$username);
  menu_begin();
  menu_entries();
  menu_end();
  body_middle();
}

function page_end(): void {
  body_end();
  html_end();
}

function body_middle(string $title=""): void {
	echo <<<EOL
	<main id="content">
	<div class="content-title">$title</div>
	EOL;
}

function body_end(): void {
	echo '</main>';
}

function title(string $title): void {
	echo '<div id="title"><span class="title-text">'.$title.'</span></div>';
}

function menu_begin(string $title="Administration", string $style="menu"): void {
	echo <<<EOL
	<div id="$style">
	<div id="menu-title">$title</div>
	<div id="menu-content">
	EOL;
}

function menu_end(): void {
  echo <<<'EOL'
  </div>
  <div id="menu-bottom"></div>
  </div>
  EOL;
}

function session(): void {
	session_start();

	if(isset($_POST['_username']))
	{
		$data = db_sql("SELECT username, password, id FROM admins WHERE username = '".$_POST['_username']."'", false);
		
		if($data[0]!="" && $data[1]==$_POST['_password'])
		{
			$_SESSION['name'] = $_POST['_username'];
			$_SESSION['username'] = $data[0];
			$_SESSION['userid'] = $data[2];
			$_SESSION['login_state'] = "TRUE";
		}
  }

  if(!array_key_exists('name', $_SESSION) && $_SESSION['login_state']!="TRUE")
	{
		html_begin("Login");
    	title("admin.winterhart.ch");
	   menu_begin("User login", "menu-login");
		echo "<form accept-charset=\"utf-8\" method=\"post\" action=\"".$_SERVER['PHP_SELF']."\">";
		echo "<input name=\"_username\"><br>";
		echo "<input name=\"_password\" type=\"password\" /><br>";
		echo "<input type=\"submit\" value=\"Login\">";
		echo "</form>";
		menu_end();
		html_end();
		exit();
  }
}
?>