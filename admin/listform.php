<?php
require('default.inc.php');
page_begin('Liste');

$fields = array(
	array("add", "RegDatum", "regdatum", date("Y-m-d H:i:s")),
	array("add", "RegPerson", "regperson", $_SESSION['userid']),
  array("text", "Gattung", "gattung", "", 50),
  array("text", "Art", "art", "", 50),
  array("text", "Sorte", "sorte", "", 50),
  array("text", "Deutscher Name", "dname", "", 50),
  array("text", "PLZ", "plz", "", 4),
  array("text", "Höhe in m ü. Meer", "hoehe", "", 4),
  array("text", "Pflanzjahr", "pflanzjahr", "", 4),
  array("text", "Kältzeone", "zone", "", 10),
  array("textarea", "Bemerkungen", "bemerkungen", "", 80, 5),
  array("text", "Anrede", "anrede", "", 10), 
  array("text", "Vorname", "vorname","", 40),
  array("text", "Nachname", "nachname", "", 40),
  array("text", "Adresse", "adresse", "", 40),
  array("text", "PLZ", "adrplz", "", 4),
  array("text", "Ort", "ort", "", 40),
  array("text", "E-Mail", "email", "", 40),
  array("text", "Status", "status", "1", 5)
);

echo '<h1>Liste</h1>';

db_add("form1", "liste", $fields, "");
db_mod("form1", "liste", $fields);
db_del("form1", "liste");
db_form("form1", "liste", $fields);

page_end();
?>