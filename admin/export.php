<?php
require("default.inc.php");

db_open();
session();

$file = 'winterhart-db-export-'.date("d.m.Y").'.csv';

header('Content-Type: text/plain; charset=utf-8');
header('Content-Disposition: attachment;filename='.$file);
header('Pragma: no-cache');
header('Expires: 0');

function show($data)
{
  printf("$data[0]\t\"$data[1]\"\t$data[2]\t\"$data[3]\"\t\"$data[4]\"\t\"$data[5]\"\t\"$data[6]\"\t$data[7]\t$data[8]\t$data[9]\t\"$data[10]\"\t\"$data[11]\"\t\"$data[12]\"\t\"$data[13]\"\t\"$data[14]\"\t\"$data[15]\"\t$data[16]\t\"$data[17]\"\t\"$data[18]\"\t$data[19]\r\n");
}

printf("ID\tDatum\tPerson\tGattung\tArt\tSorte\tDeutscher Name\tPLZ\tHöhe\tPflanzjahr\tKältezone\tBeschreibung\tAnrede\tVorname\tNachname\tAdresse\tPLZ2\tOrt\tE-Mail\tStatus\r\n");

db_sql_multi("SELECT liste.id, liste.regdatum, liste.regperson, liste.gattung, liste.art, liste.sorte, liste.dname, liste.plz, liste.hoehe, liste.pflanzjahr, liste.zone, liste.bemerkungen, liste.anrede, liste.vorname, liste.nachname, liste.adresse, liste.adrplz, liste.ort, liste.email, liste.status FROM liste", 'show'); 

?>