<?php
require('default.inc.php');
page_begin('Administration');

echo '<h1>Administration</h1>';

$username = $_SESSION['username'] ?? 'Unbekannt';

echo "<p>Herzlich Willkommen, $username</p>";

page_end();
?>