<?php
require('default.inc.php');
page_begin('Administratoren');

$felder = array(
  array("text", "Benutzername", "username", "", 15),
  array("password", "Password", "password", "", 15)
);

echo '<h1>Administratoren</h1>';

db_add("form1", "admins", $felder, "");
db_mod("form1", "admins", $felder);
db_del("form1", "admins");
db_form("form1", "admins", $felder);

page_end();
?>