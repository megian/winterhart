<?php
require('default.inc.php');
page_begin('Eintragen'); 

$felder = array(
	array("add", "RegDatum", "regdatum", date("Y-m-d H:i:s")),
	array("add", "RegPerson", "regperson", "0"),
	array("echo", "", "", "<p>In der Liste werden nur Pflanzen aufgenommen, welche mindestens 5 Jahre im Freiland in der Schweiz gewachsen sind. Wintergarten, Keller, ect. ausgeschlossen</p>", 0, 0),
	array("text", "Gattung", "gattung", "", 50),
	array("text", "Art", "art", "", 50),
	array("text", "Sorte", "sorte", "", 50),
	array("text", "Deutscher Name ", "dname", "", 50),
	array("text", "PLZ", "plz", "", 4),
	array("text", "Höhe in m ü. Meer", "hoehe", "", 4),
	array("text", "Pflanzjahr", "pflanzjahr", "", 4),
	array("textarea", "Bemerkungen", "bemerkungen", "", 80, 5),
	array("echo", "", "", "<p><br><br><b>Hinweis:</b> Die Adresse muss nicht angegeben werden. Sie ist nur zu unserer Information und wird niemals weitergegeben und/oder veröffentlicht.</p>", 0, 0),
	array("text", "Anrede", "anrede", "", 10), 
	array("text", "Vorname", "vorname","", 40),
	array("text", "Nachname", "nachname", "", 40),
	array("text", "Adresse", "adresse", "", 40),
	array("text", "PLZ", "adrplz", "", 4),
	array("text", "Ort", "ort", "", 40),
	array("text", "E-Mail", "email", "", 40),
	array("hidden", "", "status", "0", 0),
	array("echo", "", "", "<p><br><b>Bestätigung:</b> Mit dem Absenden des Formulars bestätigen Sie, dass die Informationen über den Pflanzenstandort in die Liste aufgenommen werden können, sowie das diese Informationen frei genutzt werden düfen.</p>", 0, 0)
);

echo '<h1>Eintragen</h1>';

db_add("form1", "liste", $felder, "<p>Ihre Pflanze wurde erfolgreich eingetragen. Herzlichen Dank!</p>");
db_form("form1", "liste", $felder);

page_end();
?>